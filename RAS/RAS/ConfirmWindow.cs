﻿using RAS.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RAS
{
    public partial class frmConfirm : Form
    {
        public frmConfirm()
        {
            InitializeComponent();
        }

        private void OK(object sender, EventArgs e)
        {
            Application.Run(new frmLogin());
        }

        private void CancelLogout(object sender, EventArgs e)
        {

        }
    }
}
