﻿using System;
using System.IO;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;


namespace RAS
{
    public class DBConnector
    {
        // connect string
        public string ConnectionString { get; }

        public DBConnector()
        {
            // this.ConnectionString = "Data Source=" + Path.GetFullPath("..")+"\\ras.db";
            this.ConnectionString = "Data Source=" + AppDomain.CurrentDomain.BaseDirectory + "ras.db"; ;

            if (!File.Exists(AppDomain.CurrentDomain.BaseDirectory + "ras.db"))
            {
                CreateDB();
            }
        }

        // create database and tables
        private void CreateDB()
        {
            SQLiteConnection conn = null;
            SQLiteTransaction tr = null;
            SQLiteCommand cmd = null;

            try
            {

                conn = new SQLiteConnection(this.ConnectionString);
                conn.Open();

                tr = conn.BeginTransaction();
                cmd = conn.CreateCommand();

                // 1. create user table
                // uid: primary key
                // uname: user name
                // upass: user password
                // utype: (0:customer | 1:respresentive default customer)

                cmd.Transaction = tr;
                cmd.CommandText = "DROP TABLE IF EXISTS user";
                cmd.ExecuteNonQuery();

                cmd.CommandText = @"CREATE TABLE user " +
                                        "(uid INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE," +
                                        " uname TEXT NOT NULL," +
                                        " upass TEXT NOT NULL," +
                                        " utype INTEGER DEFAULT 0 );";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "INSERT INTO user(uname,upass,utype) VALUES ('admin','admin', 1)";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "INSERT INTO user(uname,upass,utype) VALUES ('test','test', 0)";
                cmd.ExecuteNonQuery();

                // 2. create room table
                // rid: primary key
                // rno: room number
                cmd.CommandText = "DROP TABLE IF EXISTS room";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "CREATE TABLE room " +
                    "(rid INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE," +
                    " rno TEXT NOT NULL);";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "INSERT INTO room(rno) VALUES ('R 107')";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "INSERT INTO room(rno) VALUES ('R 108')";
                cmd.ExecuteNonQuery();

                // 3. reservelist table
                // rlindate: reserve check-in date primary key
                // rloutdate: reserve check-out date (check out date must greate or equal check in date)
                // rladults: how many adults
                // rstatus: room satus on check in list (0: available | 1: reserved | 2: occupied default available)
                // rid: room id reference to room id primary key
                // uid: user id reference to user id

                // case 1:
                // one people can reserve two room in one day
                // checkin date     room#       reserve to
                // 11-25-2015       R 107       Bo   ok
                // 11-25-2015       R 108       BO   ok

                // case 2:
                // one room can not reserved by two people
                // checkin date     room#       reserve to
                // 11-25-2015       R 107       Bo
                // 11-25-2015       R 107       John   x

                // one people may reserve one room for mutilple days
                // checkin date     room#       reserve to      checkout date
                // 11-25-2015       R 107       Bo              11-27-2015
                // 11-26-2015       R 107       John   x
                // 11-26-2015       R 107       Bo              by add mutilpe records in reserve table to serve the problem
                // 11-27-2015       R 107       Bo

                cmd.CommandText = "DROP TABLE IF EXISTS reservelist";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "CREATE TABLE reservelist " +
                    "(rlindate datetime NOT NULL DEFAULT current_timestamp, " +
                    " rloutdate datetime NOT NULL DEFAULT current_timestamp, " +
                    " rladults INTEGER NOT NULL DEFAULT 1," +
                    " rstatus INTEGER NOT NULL DEFAULT 0," +
                    " rid INTEGER NOT NULL," +
                    " uid INTEGER NOT NULL," +
                    " PRIMARY KEY (rlindate, rid)," +
                    " CONSTRAINT frid FOREIGN KEY(rid) REFERENCES room(rid) ON DELETE CASCADE ON UPDATE CASCADE," +
                    " CONSTRAINT fuid FOREIGN KEY(uid) REFERENCES user(uid) ON DELETE CASCADE ON UPDATE CASCADE," +
                    " CONSTRAINT crloutdate CHECK(rloutdate >= rlindate));";
                cmd.ExecuteNonQuery();

                tr.Commit();
                // Console.WriteLine("database create sucessfully.");

            }
            catch (SQLiteException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

                if (tr != null)
                {
                    try
                    {
                        tr.Rollback();
                    }
                    catch (SQLiteException ex2)
                    {
                        Console.WriteLine("Transaction rollback failed.");
                        Console.WriteLine("Error: {0}", ex2.ToString());
                    }
                    finally
                    {
                        tr.Dispose();
                    }
                }
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }

                if (tr != null)
                {
                    tr.Dispose();
                }

                if (conn != null)
                {
                    try
                    {
                        conn.Close();
                    }
                    catch (SQLiteException ex)
                    {
                        Console.WriteLine("Closing connection failed.");
                        Console.WriteLine("Error: {0}", ex.ToString());
                    }
                    finally
                    {
                        conn.Dispose();
                    }
                }
            }
        }

        // add | update | delete
        // example:
        // string sql = "insert into user(uname, upass) values(@username, @userpass)";
        // string sql = "update user set uname=@username where uid=@uid"; 
        // string sql = "delete user where uid=@uid";

        // SQLiteParameter[] parameters = { new SQLiteParameter("@username", DbType.String, 50) };
        // parameters[0].Value = txtsusername.Text
        // parameters[1].Value = txtuserpass.Text

        public int ExecuteNonQuery(string sql, SQLiteParameter[] parameters)
        {
            int affectedRows = 0;
            using (SQLiteConnection conn = new SQLiteConnection(this.ConnectionString))
            {
                conn.Open();
                using (DbTransaction transaction = conn.BeginTransaction())
                {
                    using (SQLiteCommand command = new SQLiteCommand(conn))
                    {
                        command.CommandText = sql;
                        if (parameters != null)
                        {
                            command.Parameters.AddRange(parameters);
                        }
                        affectedRows = command.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
            }
            return affectedRows;
        }

        // Select for datareader
        public SQLiteDataReader ExecuteReader(string sql, SQLiteParameter[] parameters)
        {
            SQLiteConnection connection = new SQLiteConnection(this.ConnectionString);
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            if (parameters != null)
            {
                command.Parameters.AddRange(parameters);
            }
            connection.Open();
            return command.ExecuteReader(CommandBehavior.CloseConnection);
        }

        // select for datatable
        public DataTable ExecuteDataTable(string sql, SQLiteParameter[] parameters)
        {
            using (SQLiteConnection connection = new SQLiteConnection(this.ConnectionString))
            {
                using (SQLiteCommand command = new SQLiteCommand(sql, connection))
                {
                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters);
                    }
                    SQLiteDataAdapter adapter = new SQLiteDataAdapter(command);
                    DataTable data = new DataTable();
                    adapter.Fill(data);
                    return data;
                }
            }
        }
    }

}
