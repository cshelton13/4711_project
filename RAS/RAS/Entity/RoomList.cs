﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAS.Entity
{

    public class RoomList
    {
        public DateTime CheckinDate { get; set; }

        public DateTime CheckoutDate { get; set; }

        public int Adults { get; set; }

        public int RoomStatus { get; set; } = (int) Constant.Status.Available;

        public int RoomNo { get; set; }

        public int User { get; set; }
    }
}
