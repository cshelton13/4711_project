﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAS.Entity
{


    public class User
    {
        public int Uid { get; set; }

        public string Uname { get; set; }

        public string Upass { get; set; }

        public int Utype { get; } = (int) Constant.Type.Customer;
    }
}
