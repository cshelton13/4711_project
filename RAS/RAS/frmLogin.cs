﻿using RAS.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RAS
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }
        private void login(object sender, EventArgs e)
        {
            string username = txtUserName.Text.Trim();
            string userpass = txtPassword.Text.Trim();
            LoginController lc = new LoginController();
            bool result = lc.verify(username, userpass);
            if (result)
                this.Hide();
            else
            {
                MessageBox.Show(this, "Error: Incorrect username or password.", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtUserName.Clear();
                txtPassword.Clear();
                txtUserName.Focus();
            }
        }
    }
}
