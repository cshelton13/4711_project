﻿using RAS.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Windows.Forms;
/*
Login controller allows a user to login to the system
If they are a customer, the reserve room form is displayed
If they are a representative, the check in form is displayed.
*/

namespace RAS
{
    class LoginController
    {
        public bool verify(string username, string userpass)
        {
            string sql = "SELECT uname, upass, utype " +
                            "FROM user " +
                            "WHERE uname=@username AND upass=@userpass";

            SQLiteParameter[] parameters = { new SQLiteParameter("@username", DbType.String, 50), new SQLiteParameter("@userpass", DbType.String, 50) };
            parameters[0].Value = username;
            parameters[1].Value = userpass;

            DBConnector db = new DBConnector();
            SQLiteDataReader dr = db.ExecuteReader(sql, parameters);

            if (dr.HasRows)
            {
                //  MessageBox.Show("scuess");
                User user = new User();

                while (dr.Read())
                {
                    if (Convert.ToInt32(dr["utype"]) == (int)Constant.Type.Customer)
                    {
                        // customer
                        frmReserve frm = new frmReserve(user);
                        //this.Hide();
                        frm.Show();

                    }
                    else
                    {
                        // representative
                        frmCheckin frm = new frmCheckin(user);
                        //this.Hide();
                        frm.Show();
                    }
                }
                return true;
            }
            else
                return false;
        }
    }
}
