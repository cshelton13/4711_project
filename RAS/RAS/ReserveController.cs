﻿using RAS.Entity;
using System;
using System.Data;
using System.Data.SQLite;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/*
Reserve controller checks to see if a room is available
for the provided date range and number of occupants
*/

namespace RAS
{
    class ReserveController
    {
        public bool reserve(RoomList room)
        {
            DBConnector db = new DBConnector();

            /*
            string getRoom = "SELECT rid FROM room WHERE rid != (SELECT rid FROM reservelist WHERE rloutdate < checkin)";
            SQLiteParameter[] empty = { };
            SQLiteDataReader dr = db.ExecuteReader(getRoom, empty);
            if (dr.HasRows)
            */

            string insertReservation = "INSERT INTO reservelist(rlindate,rloutdate,rladults,rstatus,uid) VALUES (@checkin,@checkout,@oc,1,@user)";
            SQLiteParameter[] parameters = { new SQLiteParameter("@checkin", DbType.DateTime),
                new SQLiteParameter("@checkout", DbType.DateTime),
                new SQLiteParameter("@oc", DbType.Int32),
                new SQLiteParameter("@user", DbType.Int32) };
            parameters[0].Value = room.CheckinDate;
            parameters[1].Value = room.CheckoutDate;
            parameters[2].Value = room.Adults;
            parameters[3].Value = room.User;

            int success = db.ExecuteNonQuery(insertReservation, parameters);
            if (success != 0)
                return true;
            else
                return false;
        }
    }
}
