﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/*
Logout controller allows any user to logout to the login window
if the operation is cancelled user is returned to the current form.
*/

namespace RAS
{
    class LogoutController
    {
        public void logout()
        {
            DialogResult dr = MessageBox.Show("Are you sure you would like to log out of the system?", "Confirm Logout",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (dr == DialogResult.OK)
            {
                Application.Run(new frmLogin());
            }
        }
    }
}
