﻿using RAS.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RAS
{
    public partial class frmReserve : Form
    {
        private User cus;

        public frmReserve(User cus)
        {
            InitializeComponent();
            this.cus = cus;
        }

        private void frmReserve_Load(object sender, EventArgs e)
        {
            this.Show();
        }
        private void logOut(object sender, EventArgs e)
        {
            Application.Run(new frmConfirm());
        }

        private void reserve(object sender, EventArgs e)
        {
            ReserveController rc = new ReserveController();
            int oc;
            if (int.TryParse(txtOccupants.Text, out oc))
            {
                RoomList room = new RoomList();
                room.Adults = oc;
                room.CheckinDate = dateTimePicker1.Value;
                room.CheckoutDate = dateTimePicker2.Value;
                room.User = cus.Uid;
                if (rc.reserve(room))
                {
                    MessageBox.Show(this, "Reservation successful from "+ room.CheckinDate + " to " + room.CheckoutDate + ".", "Reservation Made",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Application.Exit();
                }
                else
                    MessageBox.Show(this, "Sorry. No rooms are available from " + room.CheckinDate + " to " + room.CheckoutDate + ".", "Reservation Failed",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(this, "Incorrect input, please try again.", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtOccupants.Clear();
                txtOccupants.Focus();
            }

        }
    }
}
